# lu4r\_android\_app

An Automatic Speech Recognition (ASR) engine allows to transcribe a spoken utterance into one or more transcriptions. In the [LU4R][luar] framework, ASR is performed through an *ad-hoc* Android application, the **LU4R Android app**. Figures below show some of the capabilities of the LU4R Android app.

Home             |  Fragments | Virtual Joypad | Speech interface
--- | --- | --- | ---
![](./imgs/app1.png)  |  ![](./imgs/app2.png) | ![](./imgs/app3.png) | ![](./imgs/app4.png)

It relies on the official *Google ASR APIs*, that offer valuable performances for an off-the-shelf solution. 

The free-form ASR can be executed either in a continuous recognition setting or in a push-to-talk configuration. This preference can be changed in the Settings panel

The main requirement of this solution is that the device hosting the software must feature an Internet connection, in order to provide transcriptions for the spoken utterance.

The App can be deployed on both Android smart-phones and tablets.
In the latter case, even though the communication protocol remains the same, the tablet will be part of the robotic platform. The tablet can be provided with a directional condenser microphone and speakers.

The communication with the entire system is realized through TCP Sockets.
In this setting, the LU4R Android app implements a TCP Client, feeding LU4R with lists of hypotheses through a middle-layer. To this end, the **LU4R ROS interface**[^ros] has been integrated in the loop, acting as the TCP Server.

Once a new sentence is uttered by the user, this component outputs a list of hypothesized transcriptions, that are forwarded to the LU4R ROS interface.

In addition, the LU4R Android app features a virtual joypad, for tele-operating the robot. In this way, this tool provides a complete system for controlling a robotic platform.

[luar]: http://sag.art.uniroma2.it/lu4r.html "LU4R"
[^ros]: https://gitlab.com/andreavanzo/android_interface