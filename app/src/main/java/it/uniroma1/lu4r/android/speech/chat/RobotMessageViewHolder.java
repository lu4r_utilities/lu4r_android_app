package it.uniroma1.lu4r.android.speech.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import it.uniroma1.lu4r.R;

/**
 * Created by nduccio on 07/07/17.
 */

public class RobotMessageViewHolder extends RecyclerView.ViewHolder {

    protected RelativeLayout linearLayout;
    protected ImageView imageView;
    protected TextView textView;
    protected TextView timeView;

    public RobotMessageViewHolder(View view) {
        super(view);
        this.linearLayout = (RelativeLayout) view.findViewById(R.id.robot_turn_layout);
        this.imageView = (ImageView) view.findViewById(R.id.robot_image_view);
        this.textView = (TextView) view.findViewById(R.id.robot_text_view);
        this.timeView = (TextView) view.findViewById(R.id.robot_time_text_view);
    }
}
