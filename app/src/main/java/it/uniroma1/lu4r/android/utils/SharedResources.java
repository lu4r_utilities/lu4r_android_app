package it.uniroma1.lu4r.android.utils;

import java.util.Locale;

/**
 * Created by nduccio on 18/10/17.
 */

public class SharedResources {

    /*
        Preferences
     */
    public static String IP_ADDRESS = "127.0.0.1";
    public static int PORT = 4567;
    public static String CONNECTION_TYPE = "wifi";

    public static String JOY_SEPARATOR = " ";

    public static boolean CONTINUOUS_ASR = false;
    public static Locale STT_LANG = Locale.getDefault();
    public static boolean OFFLINE_ASR = false;
    public static boolean WIFI_ONLY = false;
    public static boolean DEBUG = false;
    public static boolean LOG_RECORDING = false;


    public static String TTS_EXAMPLE_SENTENCE = "This is an example of speech synthesis in English";
    public static float TTS_PITCH = 1.8f;
    public static float TTS_RATE = 2.0f;
    public static Locale TTS_LANG = Locale.getDefault();

}
