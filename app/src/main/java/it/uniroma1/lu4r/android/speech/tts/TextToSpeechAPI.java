package it.uniroma1.lu4r.android.speech.tts;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

import it.uniroma1.lu4r.android.speech.SpeechInterfaceFragment;
import it.uniroma1.lu4r.android.utils.SharedResources;

/**
 * Created by nduccio on 23/10/17.
 */

public class TextToSpeechAPI {

    private TextToSpeech tts = null;

    public TextToSpeechAPI(Context c) {
        if (tts == null) {
            tts = new TextToSpeech(c,
                    new TextToSpeech.OnInitListener() {
                        @Override
                        public void onInit(int status) {

                        }
                    });
            tts.setPitch(SharedResources.TTS_PITCH);
            tts.setSpeechRate(SharedResources.TTS_RATE);
        }
    }

    public void speak(String message) {
        if (SpeechInterfaceFragment.getContinuousSpeechAPI() != null) {
            if (SpeechInterfaceFragment.getContinuousSpeechAPI().isActive()) {
                SpeechInterfaceFragment.getContinuousSpeechAPI().stopListening();
                tts.speak(message, TextToSpeech.QUEUE_FLUSH, null, message.hashCode() + "");
                SpeechInterfaceFragment.getContinuousSpeechAPI().startListening();
            } else {
                tts.speak(message, TextToSpeech.QUEUE_FLUSH, null, message.hashCode() + "");
            }
        } else {
            tts.speak(message, TextToSpeech.QUEUE_FLUSH, null, message.hashCode() + "");
        }
    }

    public void setPitch(float pitch) {
        tts.setPitch(pitch);
    }

    public void setSpeechRate(float rate) {
        tts.setSpeechRate(rate);
    }

    public boolean isSpeaking() {
        return tts.isSpeaking();
    }

    public void setLanguage(Locale language) {
        tts.setLanguage(language);
    }
}
