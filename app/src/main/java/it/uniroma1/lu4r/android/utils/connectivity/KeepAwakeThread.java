package it.uniroma1.lu4r.android.utils.connectivity;

import it.uniroma1.lu4r.android.main.MainActivity;

/**
 * Created by nduccio on 06/06/16.
 */
public class KeepAwakeThread extends Thread {
    private volatile boolean running = true;

    public void terminate() {
        running = false;
    }

    public void run() {
        while (running) {
            MainActivity.getClient().send("KEEP_AWAKE");
            synchronized (this) {
                try {
                    wait(20000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
