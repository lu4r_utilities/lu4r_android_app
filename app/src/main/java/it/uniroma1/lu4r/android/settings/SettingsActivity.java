package it.uniroma1.lu4r.android.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import it.uniroma1.lu4r.R;
import it.uniroma1.lu4r.android.main.MainActivity;
import it.uniroma1.lu4r.android.utils.SharedResources;

import java.util.Locale;

/**
 * Created by nduccio on 21/01/15.
 */
public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG = SettingsActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i(TAG, "onSharedPreferenceChanged");
        applySingleSetting(sharedPreferences, key);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void applySingleSetting(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case "ip_address":
                SharedResources.IP_ADDRESS = sharedPreferences.getString(key, "127.0.0.1");
                break;
            case "port":
                SharedResources.PORT = Integer.parseInt(sharedPreferences.getString(key, "4567"));
                break;
            case "connection":
                SharedResources.CONNECTION_TYPE = sharedPreferences.getString(key, "wifi");
                break;
            case "joy_params":
                SharedResources.JOY_SEPARATOR = sharedPreferences.getString(key, " ");
                break;
            case "continuous_speech":
                SharedResources.CONTINUOUS_ASR = sharedPreferences.getBoolean(key, true);
                Log.i(TAG, SharedResources.CONTINUOUS_ASR + "");
                break;
            case "speech_language":
                String sttLanguage = sharedPreferences.getString(key, "default");
                switch(sttLanguage) {
                    case "default":
                        SharedResources.STT_LANG = Locale.getDefault();
                        break;
                    case "en":
                        SharedResources.STT_LANG = Locale.ENGLISH;
                        break;
                    case "it":
                        SharedResources.STT_LANG = Locale.ITALIAN;
                        break;
                    case "fr":
                        SharedResources.STT_LANG = Locale.FRENCH;
                        break;
                    case "de":
                        SharedResources.STT_LANG = Locale.GERMAN;
                        break;
                }
                break;
            case "tts_language":
                String ttsLanguage = sharedPreferences.getString(key, "default");
                if (ttsLanguage.equals("default")) {
                    MainActivity.getTTS().setLanguage(Locale.getDefault());
                } else {
                    MainActivity.getTTS().setLanguage(new Locale(ttsLanguage));
                    SharedResources.TTS_LANG = new Locale(ttsLanguage);
                    switch(ttsLanguage) {
                        case "default":
                            SharedResources.TTS_EXAMPLE_SENTENCE = getString(R.string.ex_en);
                            break;
                        case "en_UK":
                            SharedResources.TTS_EXAMPLE_SENTENCE = getString(R.string.ex_en);
                            break;
                        case "en_US":
                            SharedResources.TTS_EXAMPLE_SENTENCE = getString(R.string.ex_en);
                            break;
                        case "it":
                            SharedResources.TTS_EXAMPLE_SENTENCE = getString(R.string.ex_it);
                            break;
                        case "fr":
                            SharedResources.TTS_EXAMPLE_SENTENCE = getString(R.string.ex_fr);
                            break;
                        case "es":
                            SharedResources.TTS_EXAMPLE_SENTENCE = getString(R.string.ex_es);
                            break;
                        case "de":
                            SharedResources.TTS_EXAMPLE_SENTENCE = getString(R.string.ex_de);
                            break;
                    }
                }
                MainActivity.getTTS().speak(SharedResources.TTS_EXAMPLE_SENTENCE);
                break;
            case "offline_speech":
                SharedResources.OFFLINE_ASR = sharedPreferences.getBoolean(key, true);
                break;
            case "wifi_speech":
                SharedResources.WIFI_ONLY = sharedPreferences.getBoolean(key, true);
                break;
            case "debug_speech":
                SharedResources.DEBUG = sharedPreferences.getBoolean(key, false);
                break;
            case "recording_preference":
                SharedResources.LOG_RECORDING = sharedPreferences.getBoolean(key, false);
                break;
            case "pitchSeekBar":
                SharedResources.TTS_PITCH = ((float)sharedPreferences.getInt("pitchSeekBar", 1)) / 10.0f;
                MainActivity.getTTS().setPitch(SharedResources.TTS_PITCH);
                Toast.makeText(getApplicationContext(), "Pitch: " + SharedResources.TTS_PITCH, Toast.LENGTH_SHORT).show();
                MainActivity.getTTS().speak(SharedResources.TTS_EXAMPLE_SENTENCE);
                break;
            case "rateSeekBar":
                SharedResources.TTS_RATE = ((float)sharedPreferences.getInt("rateSeekBar", 1)) / 10.0f;
                MainActivity.getTTS().setSpeechRate(SharedResources.TTS_RATE);
                Toast.makeText(getApplicationContext(), "Rate: " + SharedResources.TTS_RATE, Toast.LENGTH_SHORT).show();
                MainActivity.getTTS().speak(SharedResources.TTS_EXAMPLE_SENTENCE);
                break;
        }
    }
}
