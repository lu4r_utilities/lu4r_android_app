package it.uniroma1.lu4r.android.main;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.uniroma1.lu4r.R;
import it.uniroma1.lu4r.android.speech.tts.TextToSpeechAPI;
import it.uniroma1.lu4r.android.utils.SharedResources;
import it.uniroma1.lu4r.android.utils.connectivity.TCPConnectionService;
import it.uniroma1.lu4r.android.joypad.JoypadFragment;
import it.uniroma1.lu4r.android.home.HomeFragment;
import it.uniroma1.lu4r.android.speech.SpeechInterfaceFragment;
import it.uniroma1.lu4r.android.settings.SettingsActivity;
import it.uniroma1.lu4r.android.speech.chat.model.Message;


public class MainActivity extends FragmentActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    public static ArrayList<Message> sentences = new ArrayList<>();
    private static Fragment frag = null;
    private static TCPConnectionService client = null;
    private static TextToSpeechAPI tts = null;
    private static String activeFragment = "";
    protected PowerManager.WakeLock mWakeLock;
    private static final String TAG = MainActivity.class.getName();

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        SharedResources.IP_ADDRESS = sharedPreferences.getString("ip_address", "127.0.0.1");
        SharedResources.PORT = Integer.parseInt(sharedPreferences.getString("port", "4567"));
        SharedResources.CONNECTION_TYPE = sharedPreferences.getString("connection", "wifi");

        SharedResources.JOY_SEPARATOR = sharedPreferences.getString("joy_params", " ");

        SharedResources.CONTINUOUS_ASR = sharedPreferences.getBoolean("continuous_speech", true);
        SharedResources.STT_LANG = new Locale(sharedPreferences.getString("speech_language", Locale.getDefault().toString()));
        SharedResources.OFFLINE_ASR = sharedPreferences.getBoolean("offline_speech", true);
        SharedResources.WIFI_ONLY = sharedPreferences.getBoolean("wifi_speech", true);
        SharedResources.DEBUG = sharedPreferences.getBoolean("debug_speech", false);
        SharedResources.LOG_RECORDING = sharedPreferences.getBoolean("recording_preference", false);


        SharedResources.TTS_PITCH = ((float)sharedPreferences.getInt("pitchSeekBar", 1)) / 10.0f;
        SharedResources.TTS_RATE = ((float)sharedPreferences.getInt("rateSeekBar", 1)) / 10.0f;
        SharedResources.TTS_LANG = new Locale(sharedPreferences.getString("tts_language", Locale.getDefault().toString()));


        //SharedResources.CONNECTION_TYPE

        if (SharedResources.CONNECTION_TYPE.equals("wifi") && client == null) {
            client = new TCPConnectionService(this);
        }
        setContentView(R.layout.activity_main);

        //Init drawer
        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        if (tts == null) {
            tts = new TextToSpeechAPI(this.getApplicationContext());
        }

        requestPermissions();

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();
    }

    /**
     * Receiving speech input
     * */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        String sectionName = "";
        switch(position) {
            case 0:
                //Home
                frag = HomeFragment.newInstance(position + 1);
                sectionName = getString(R.string.title_section1);
                activeFragment = "$HOME";
                break;
            case 1:
                //Joypad
                frag = JoypadFragment.newInstance(position + 1);
                sectionName = getString(R.string.title_section2);
                activeFragment = "$JOY";
                break;
            case 2:
                //SpeechInterface
                frag = SpeechInterfaceFragment.newInstance(position + 1);
                sectionName = getString(R.string.title_section3);
                activeFragment = "$SLU";
                break;
        }
        fragmentTransaction.replace(R.id.container, frag, activeFragment);
        fragmentTransaction.commit();
        Toast.makeText(getApplicationContext(), sectionName, Toast.LENGTH_SHORT).show();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    @Override
    public void onDestroy() {
        this.mWakeLock.release();
        super.onDestroy();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = 0.0f;
        getWindow().setAttributes(lp);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar == null) throw new AssertionError();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mNavigationDrawerFragment == null) {
            mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        }
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = 1.0f;
        getWindow().setAttributes(lp);
    }

    public void requestPermissions() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Microphone");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MainActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(MainActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public static TCPConnectionService getClient() {
        return client;
    }

    public static TextToSpeechAPI getTTS() {
        return tts;
    }

    public String getActiveFragmentString() {
        return activeFragment;
    }

    public static Fragment getActiveFragment() {
        return frag;
    }

}
