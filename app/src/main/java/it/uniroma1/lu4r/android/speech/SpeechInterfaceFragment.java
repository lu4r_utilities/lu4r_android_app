package it.uniroma1.lu4r.android.speech;

import android.app.Activity;
import android.app.Fragment;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.res.ResourcesCompat;
//TODO
//import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.io.File;

import it.uniroma1.lu4r.R;
import it.uniroma1.lu4r.android.main.MainActivity;
import it.uniroma1.lu4r.android.utils.SharedResources;
import it.uniroma1.lu4r.android.speech.asr.ContinuousSpeechAPI;
import it.uniroma1.lu4r.android.speech.asr.GoogleSpeechAPI;
import it.uniroma1.lu4r.android.speech.chat.model.Message;
import it.uniroma1.lu4r.android.speech.chat.SpeechInterfaceViewAdapter;

import static android.widget.Toast.makeText;

/**
 * A {@link Fragment} that creates two Speech Listener interfaces to be used by the user.<p/>
 * One Speech Listener is the PocketSphinx API, that listens continuously for keywords/keyphrases.<p/>
 * The other is Google Speech Recognition, that can either be used as Push-To-Talk interface or as phrase recognizer that gets called after PocketSphinx recognizes a keyword.<p/>
 * */
public class SpeechInterfaceFragment extends Fragment {

    public static final String ARG_SECTION_NUMBER = "section_number";

    private final String TAG = SpeechInterfaceFragment.class.getName();

    private RecyclerView mRecyclerView;
    private SpeechInterfaceViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    private static FloatingActionButton fab;

    private AudioManager mAudioManager;
    private int mStreamVolume = 0;

    //TODO
    //private NotificationCompat.Builder mBuilder;
    private int mNotificationId = 001;
    private long lastTouch = 3000;

    private GoogleSpeechAPI googleSpeechAPI;
    private static ContinuousSpeechAPI continuousSpeechAPI;

    public static SpeechInterfaceFragment newInstance(int sectionNumber) {
        SpeechInterfaceFragment fragment = new SpeechInterfaceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public SpeechInterfaceFragment() {

    }

    /**
     * At creation, all the preferences are loaded into private variables, and the floating action button is created (together with the event listeners for touch/click)<p/>
     * */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        Log.i(TAG, SharedResources.CONTINUOUS_ASR + "");
        super.onCreate(savedInstanceState);
        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mStreamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        //TODO
        /*mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getActivity().getApplicationContext())
                .setSmallIcon(R.drawable.mic_icon)
                .setContentTitle("LU4R Android app")
                .setContentText("Continuous speech recognition running: currently using the microphone");*/

        String path="SpeechToRobot";
        createDirIfNotExists(path);
    }

    /**
     * Function called at fragment creation.<p/>
     * If the app is connected to the sever python, a message with the fragment name gets sent.<p/>
     * This function takes care of initialising the google and pocketsphinx api, and inits also the view for hypothesis printing.<p/>
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        Log.i(TAG, SharedResources.CONTINUOUS_ASR + "");
        View view = inflater.inflate(R.layout.fragment_speech_interface, container, false);

        fab = view.findViewById(R.id.fab);
        fab.hide(false);
        fab.show(true);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked(v);
            }
        });

        fab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return touched(event);
            }
        });

        layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new SpeechInterfaceViewAdapter(getActivity(), MainActivity.sentences);
        mRecyclerView.setAdapter(adapter);

        if (MainActivity.getClient().isConnected()) {
            MainActivity.getClient().send("$SLU");
        }
        if (this.googleSpeechAPI == null)
            this.googleSpeechAPI = new GoogleSpeechAPI(this, getActivity().getApplicationContext(), view);

        if (this.continuousSpeechAPI == null)
            this.continuousSpeechAPI = new ContinuousSpeechAPI(this, view);
        //fabButton.showFloatingActionButton();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (SharedResources.CONTINUOUS_ASR) {
            this.fab.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.continuous_speech_icon, null));
        } else {
            this.fab.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.mic_icon, null));
        }
        fab.hide(false);
        fab.show(true);
    }

    /**
     * Function that handles the destruction of the fragment.<p/>
     * The tasks in background are stopped, audio is set again at its original value, any notification is removed.<p/>
     * */
    @Override
    public void onDestroy() {
        super.onDestroy();

        fab.hide(true);

        this.googleSpeechAPI.destroy();
        this.continuousSpeechAPI.destroy();

        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mStreamVolume, 0);

        if(this.continuousSpeechAPI.isActive()) {
            NotificationManager mNotifyMgr = (NotificationManager) getActivity().getSystemService(getActivity().getApplicationContext().NOTIFICATION_SERVICE);
            mNotifyMgr.cancel(mNotificationId);
        }

        this.googleSpeechAPI = null;
        this.continuousSpeechAPI = null;
        super.onDestroy();
    }



    /**
     * Function called at startup. Checks if the app directory exists on the SD or not.
     * */
    public boolean createDirIfNotExists(String path) {
        boolean ret = true;

        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            makeText(getActivity().getApplicationContext(), "Problem in reading the SD, keyword database actions will not be available.", Toast.LENGTH_SHORT).show();
            Log.wtf(TAG, Environment.getExternalStorageState());
            return false;
        }

        File file = new File(Environment.getExternalStorageDirectory(), path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e(TAG, "Problem creating App folder");
                ret = false;
            }
        }
        return ret;
    }

    /**
     * Function called when the button is pushed and useContinuous mode is active.<p/>
     * If the Async Task has not started, start it (check if mic is busy).<p/>
     * If the Task is active, stop it.<p/>
     * The function will show a couple of Toast to signal the user that the choice has been successfully performed.<p/>
     * */
    public void clicked(View view) {
        if (SharedResources.CONTINUOUS_ASR) { //If we are in push to talk mode we don't care about sphinx
            NotificationManager mNotifyMgr = (NotificationManager) getActivity().getSystemService(getActivity().getApplicationContext().NOTIFICATION_SERVICE);

            if (this.continuousSpeechAPI.isActive()) {
                makeText(getActivity().getApplicationContext(), "Deactivating continuous ASR...", Toast.LENGTH_SHORT).show();
                mNotifyMgr.cancel(mNotificationId);
                this.continuousSpeechAPI.stopListening();
            } else {
                if (checkIfMicrophoneIsBusy(getActivity().getApplicationContext())) {
                    makeText(getActivity().getApplicationContext(), "Activating continuous ASR...", Toast.LENGTH_SHORT).show();
                    //TODO
                    //mNotifyMgr.notify(mNotificationId, mBuilder.build());
                    this.continuousSpeechAPI.startListening();
                } else {
                    makeText(getActivity().getApplicationContext(), "Microphone is busy, retry in few seconds", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public boolean touched(MotionEvent event) {
        long currTime = event.getEventTime();
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if ((ni == null && !SharedResources.OFFLINE_ASR) || ni.getType() != ConnectivityManager.TYPE_WIFI && ni.getType() != ConnectivityManager.TYPE_MOBILE || (ni.getType() == ConnectivityManager.TYPE_MOBILE && SharedResources.WIFI_ONLY)) {
            if (SharedResources.DEBUG) { //Write error cause if debug enabled
                if (currTime - lastTouch > 1500) {
                    lastTouch = currTime;
                    makeText(getActivity().getApplicationContext(), "Error: check Wifi/3G connection", Toast.LENGTH_SHORT).show();
                }
            }
            return true;
        } else {
            if (SharedResources.CONTINUOUS_ASR) {
                return false;
            }
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    fab.setColorNormalResId(R.color.recording);
                    googleSpeechAPI.startListening();
                    break;

                case MotionEvent.ACTION_UP:
                    fab.setColorNormalResId(R.color.accent);
                    googleSpeechAPI.stopListening();

                    break;
            }
            return true;
        }
    }

    /**
     * Function called at button press, checks if the mic is currently busy before enabling the useContinuous listening mode.<p/>
     * Useful if the user does not give the app the time to close the async task before creating a new one.<p/>
     * */
    public static boolean checkIfMicrophoneIsBusy(Context ctx){
        AudioRecord audio = null;
        boolean ready = true;
        try{
            int baseSampleRate = 44100;
            int channel = AudioFormat.CHANNEL_IN_MONO;
            int format = AudioFormat.ENCODING_PCM_16BIT;
            int buffSize = AudioRecord.getMinBufferSize(baseSampleRate, channel, format );
            audio = new AudioRecord(MediaRecorder.AudioSource.MIC, baseSampleRate, channel, format, buffSize );
            audio.startRecording();
            short buffer[] = new short[buffSize];
            int audioStatus = audio.read(buffer, 0, buffSize);

            if(audioStatus == AudioRecord.ERROR_INVALID_OPERATION || audioStatus == AudioRecord.STATE_UNINITIALIZED /* For Android 6.0 */)
                ready = false;
        }
        catch(Exception e){
            ready = false;
        }
        finally {
            try{
                audio.release();
            }
            catch(Exception e){}
        }

        return ready;
    }

    public void updateSentences(Message sentence) {
        MainActivity.sentences.add(sentence);
        adapter = new SpeechInterfaceViewAdapter(getActivity(), MainActivity.sentences);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
    }

    public static ContinuousSpeechAPI getContinuousSpeechAPI() {
        return continuousSpeechAPI;
    }

    public static FloatingActionButton getFab() {
        return fab;
    }
}
