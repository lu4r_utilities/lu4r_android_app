package it.uniroma1.lu4r.android.utils.connectivity;

import android.app.Activity;

import it.uniroma1.lu4r.android.main.MainActivity;
import it.uniroma1.lu4r.android.speech.SpeechInterfaceFragment;
import it.uniroma1.lu4r.android.speech.chat.model.Author;
import it.uniroma1.lu4r.android.speech.chat.model.Message;

/**
 * Created by nduccio on 06/06/16.
 */
public class ResponseHandlerThread {

    private boolean isConnected = true;
    private Activity mainActivity;
    private String response;
    private long lastUpdate = -1;


    public ResponseHandlerThread(Activity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void run() {
        this.isConnected = true;
        Runnable serverTask = new Runnable() {
            @Override
            public void run() {
                    while (isConnected) {
                        response = MainActivity.getClient().waitForResponse();
                        if (response != null) {
                            response = response.trim();
                            mainActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MainActivity.getTTS().speak(response);
                                    ((SpeechInterfaceFragment) MainActivity.getActiveFragment()).updateSentences(new Message(Author.ROBOT, response));
                                }
                            });
                        }
                    }
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    public void stop() {
        this.isConnected = false;
    }

    public long getLastUpdate(){
        return lastUpdate;
    }
}
