package it.uniroma1.lu4r.android.speech.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import it.uniroma1.lu4r.R;

/**
 * Created by nduccio on 07/07/17.
 */

public class UserMessageViewHolder extends RecyclerView.ViewHolder {

    protected RelativeLayout linearLayout;
    protected ImageView imageView;
    protected TextView textView;
    protected TextView timeView;

    public UserMessageViewHolder(View view) {
        super(view);
        this.linearLayout = (RelativeLayout) view.findViewById(R.id.user_turn_layout);
        this.textView = (TextView) view.findViewById(R.id.user_text_view);
        this.timeView = (TextView) view.findViewById(R.id.user_time_text_view);
        this.imageView = (ImageView) view.findViewById(R.id.user_image_view);
    }
}
