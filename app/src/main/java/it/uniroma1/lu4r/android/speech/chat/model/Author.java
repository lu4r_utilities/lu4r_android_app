package it.uniroma1.lu4r.android.speech.chat.model;

/**
 * Created by nduccio on 07/07/17.
 */

public enum Author {

    USER, ROBOT;

}
