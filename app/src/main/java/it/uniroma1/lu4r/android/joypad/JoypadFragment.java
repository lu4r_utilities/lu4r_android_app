package it.uniroma1.lu4r.android.joypad;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import it.uniroma1.lu4r.R;
import it.uniroma1.lu4r.android.main.MainActivity;
import it.uniroma1.lu4r.android.joypad.utils.Joypad;
import it.uniroma1.lu4r.android.utils.SharedResources;

public class JoypadFragment extends Fragment {

    public static final String ARG_SECTION_NUMBER = "section_number";
    private Joypad js;
    private TextView xTextView, yTextView, angleTextView, distanceTextView, normDistanceTextView, directionTextView;


    public static JoypadFragment newInstance(int sectionNumber) {
        JoypadFragment fragment = new JoypadFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public JoypadFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_joypad, container, false);
        xTextView = view.findViewById(R.id.x);
        yTextView = view.findViewById(R.id.y);
        angleTextView = view.findViewById(R.id.angle);
        distanceTextView = view.findViewById(R.id.distance);
        normDistanceTextView = view.findViewById(R.id.normalizedDistance);
        directionTextView = view.findViewById(R.id.direction);
        RelativeLayout layout_joystick = view.findViewById(R.id.layout_joystick);
        js = new Joypad(getActivity().getApplicationContext(), layout_joystick, R.drawable.joy_ptr);
        js.setStickSize(50, 50);
        js.setLayoutAlpha(150);
        js.setStickAlpha(100);
        js.setOffset(40);
        js.setMinimumDistance(20);
        if (MainActivity.getClient().isConnected()) {
            MainActivity.getClient().send("$JOY");
        }

        layout_joystick.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                js.drawStick(arg1);
                String directionString = "";
                if(arg1.getAction() == MotionEvent.ACTION_DOWN || arg1.getAction() == MotionEvent.ACTION_MOVE) {
                    xTextView.setText("X : " + String.valueOf(js.getX()));
                    yTextView.setText("Y : " + String.valueOf(js.getY()));
                    angleTextView.setText("Angle : " + String.valueOf(js.getAngle()));
                    distanceTextView.setText("Distance : " + String.valueOf(js.getDistance()));
                    normDistanceTextView.setText("Normalized distance : " + String.valueOf(js.getNormalizedDistance()));
                    int direction = js.get8Direction();
                    if(direction == Joypad.STICK_UP) {
                        directionString = "Up";
                    } else if(direction == Joypad.STICK_UPRIGHT) {
                        directionString = "Up Right";
                    } else if(direction == Joypad.STICK_RIGHT) {
                        directionString = "Right";
                    } else if(direction == Joypad.STICK_DOWNRIGHT) {
                        directionString = "Down Right";
                    } else if(direction == Joypad.STICK_DOWN) {
                        directionString = "Down";
                    } else if(direction == Joypad.STICK_DOWNLEFT) {
                        directionString = "Down Left";
                    } else if(direction == Joypad.STICK_LEFT) {
                        directionString = "Left";
                    } else if(direction == Joypad.STICK_UPLEFT) {
                        directionString = "Up Left";
                    } else if(direction == Joypad.STICK_NONE) {
                        directionString = "Center";
                    }
                    directionTextView.setText("Direction : " + directionString);
                } else if(arg1.getAction() == MotionEvent.ACTION_UP) {
                    xTextView.setText("X :");
                    yTextView.setText("Y :");
                    angleTextView.setText("Angle :");
                    distanceTextView.setText("Distance :");
                    normDistanceTextView.setText("Normalized distance :");
                    directionTextView.setText("Direction :");
                }
                if (MainActivity.getClient().isConnected()) {
                    MainActivity.getClient().send(js.getNormalizedDistance() + SharedResources.JOY_SEPARATOR + js.getAngle());
                }

                return true;
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
