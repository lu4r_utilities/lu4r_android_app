package it.uniroma1.lu4r.android.speech.asr;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vikramezhil.droidspeech.DroidSpeech;
import com.vikramezhil.droidspeech.OnDSListener;
import com.vikramezhil.droidspeech.OnDSPermissionsListener;

import it.uniroma1.lu4r.R;
import it.uniroma1.lu4r.android.main.MainActivity;
import it.uniroma1.lu4r.android.speech.SpeechInterfaceFragment;
import it.uniroma1.lu4r.android.speech.chat.model.Author;
import it.uniroma1.lu4r.android.speech.chat.model.Message;

/**
 * Created by nduccio on 18/10/17.
 */

public class ContinuousSpeechAPI implements OnDSListener, OnDSPermissionsListener {

    private SpeechInterfaceFragment main;
    private Context c;
    private View v;
    private DroidSpeech droidSpeech;
    private boolean isActive = false;
    private static final String TAG = ContinuousSpeechAPI.class.getName();

    public ContinuousSpeechAPI(SpeechInterfaceFragment main, View view) {
        this.c = main.getActivity().getApplicationContext();
        this.v = view;
        this.main = main;
        this.droidSpeech = new DroidSpeech(this.c, null);
        this.droidSpeech.setContinuousSpeechRecognition(true);
        this.droidSpeech.setOnDroidSpeechListener(this);

    }

    public void startListening() {
        while (MainActivity.getTTS().isSpeaking()) {}
        SpeechInterfaceFragment.getFab().setColorNormalResId(R.color.recording);
        this.droidSpeech.startDroidSpeechRecognition();
        Log.i(TAG, "Start Continuous Listening");
        this.isActive = true;

    }

    public void stopListening() {
        Log.i(TAG, "Stop Continuous Listening");
        SpeechInterfaceFragment.getFab().setColorNormalResId(R.color.accent);
        this.droidSpeech.closeDroidSpeechOperations();
        this.isActive = false;
    }

    public void destroy() {
        if (this.droidSpeech != null) {
            this.droidSpeech.closeDroidSpeechOperations();
            this.droidSpeech = null;
        }
        this.isActive = false;
    }

    public boolean isActive() {
        return this.isActive;
    }

    @Override
    public void onDroidSpeechRmsChanged(float rmsChangedValue) {

    }

    @Override
    public void onDroidSpeechLiveResult(String liveSpeechResult) {
        Log.i(TAG, "Live results: " + liveSpeechResult);
    }

    @Override
    public void onDroidSpeechFinalResult(String finalSpeechResult) {
        this.main.updateSentences(new Message(Author.USER, finalSpeechResult));
        String hypoToSend = "{\"hypotheses\":[{\"transcription\":\"" + finalSpeechResult + "\",\"confidence\":0.0,\"rank\":\"1\"}]}";
        Log.i(TAG, "Sending Final Result: " + finalSpeechResult);
        MainActivity.getClient().send(hypoToSend);
    }

    @Override
    public void onDroidSpeechClosedByUser() {

    }

    @Override
    public void onDroidSpeechError(String errorMsg) {

    }

    @Override
    public void onDroidSpeechAudioPermissionStatus(boolean audioPermissionGiven, String errorMsgIfAny) {

    }
}
