package it.uniroma1.lu4r.android.speech.chat.model;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nduccio on 07/07/17.
 */

public class Message {

    private String sentence = "";
    private Author author;
    private String time = "";
    private static final DateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");

    public Message(Author author, String sentence) {
        this.author = author;
        this.sentence = sentence;
        Date date = (new Date(System.currentTimeMillis()));
        time = sdf.format(date);
    }

    public String getSentence() {
        return this.sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public Author getAuthor() {
        return this.author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(long timestamp) {
        Date date = (new Date(timestamp));
        this.time = sdf.format(date);
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTime(Date date) {
        this.time = sdf.format(date);
    }
}
