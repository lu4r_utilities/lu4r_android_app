package it.uniroma1.lu4r.android.speech.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import it.uniroma1.lu4r.R;
import it.uniroma1.lu4r.android.speech.chat.model.Author;
import it.uniroma1.lu4r.android.speech.chat.model.Message;

/**
 * Created by nduccio on 07/07/17.
 */

public class SpeechInterfaceViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Message> feedItemList;
    private Context mContext;

    public SpeechInterfaceViewAdapter(Context context, List<Message> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        switch (i) {
            case 0:
                view = LayoutInflater.from(this.mContext).inflate(R.layout.user_turn_layout, null);
                viewHolder = new UserMessageViewHolder(view);
                break;
            case 1:
                view = LayoutInflater.from(this.mContext).inflate(R.layout.robot_turn_layout, null);
                viewHolder = new RobotMessageViewHolder(view);
                break;
            default:
                view = LayoutInflater.from(this.mContext).inflate(R.layout.user_turn_layout, null);
                viewHolder = new UserMessageViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case 0:
                UserMessageViewHolder vh1 = (UserMessageViewHolder) viewHolder;
                configureUserViewHolder(vh1, position);
                break;
            case 1:
                RobotMessageViewHolder vh2 = (RobotMessageViewHolder) viewHolder;
                configureRobotViewHolder(vh2, position);
                break;
        }
    }

    private void configureUserViewHolder(UserMessageViewHolder vh1, int position) {
        Message feedItem = feedItemList.get(position);
        vh1.textView.setText(feedItem.getSentence());
        vh1.timeView.setText(feedItem.getTime());
    }

    private void configureRobotViewHolder(RobotMessageViewHolder vh2, int position) {
        Message feedItem = feedItemList.get(position);
        vh2.textView.setText(feedItem.getSentence());
        vh2.timeView.setText(feedItem.getTime());
    }

    @Override
    public int getItemViewType(int position) {
        Author message_type = feedItemList.get(position).getAuthor();
        switch (message_type) {
            case USER:
                return 0;
            case ROBOT:
                return 1;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

}
