package it.uniroma1.lu4r.android.utils.connectivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import static android.widget.Toast.makeText;

/**
 * Created by nduccio on 23/03/15.
 */

public class TCPConnectionService extends AsyncTask<Void, Void, Void> {

    private Socket clientSocket = null;
    private PrintWriter out = null;
    private boolean isConnected = false;
    private KeepAwakeThread kat;
    private ResponseHandlerThread lt = null;
    private Activity mainActivity;
    private String TAG = this.getClass().getName();



    public TCPConnectionService(Activity mainActivity){
        this.mainActivity = mainActivity;
    }

    public boolean connect(String addr, int port) {

        try {
            if (isConnected) {
                return true;
            } else {
                clientSocket = new Socket();
                clientSocket.connect(new InetSocketAddress(addr, port), 5000);
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                isConnected = true;
                kat = new KeepAwakeThread();
                kat.start();
                lt = new ResponseHandlerThread(this.mainActivity);
                lt.run();
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            isConnected = false;
            return false;
        }
    }

    public boolean disconnect() {
        if (clientSocket != null && clientSocket.isConnected()) {
            try {
                clientSocket.close();
                isConnected = false;
                kat.terminate();
                lt.stop();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                isConnected = false;
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

    public boolean send(String msg) {
        if (out != null) {
            if(msg.contains("REQ") ) {
                out.println(msg);
                out.flush();
                Log.i(TAG, "Sending message: " + msg);
                return true;
            }
            if( this.checkConnection() == false){
                makeText(mainActivity.getApplicationContext(), "Error: Unable to send message to the server", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Error: Unable to send message to the server");
                return false;
            }else{
                out.println(msg);
                out.flush();
                Log.i(TAG, "Sending message: " + msg);
                return true;

            }

        }else{
            makeText(mainActivity.getApplicationContext(), "Error: Unable to send message to the server", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "Error: Unable to send message to the server asdasd");
            return false;
        }
    }

    public String readResponse() {
        BufferedReader stdIn;
        String response;
        try {
            stdIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            response = stdIn.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return response;
    }

    public String waitForResponse() {
        try {
            String response = readResponse();
            if (response != null)
                while (response.isEmpty())
                    response = readResponse();
            return response;
        } catch (NullPointerException npe) {
            Log.i("NullPointerException", "Connection closed!");
            return null;
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    public boolean checkConnection() {
        if(isConnected){

            long start = System.currentTimeMillis();
            long delta_t_before = start -lt.getLastUpdate();
            if (delta_t_before >= 3000) {
                send("REQ");
                while (System.currentTimeMillis() - start <= 500) {
                    long delta_t_now = lt.getLastUpdate() - start;
                    if (delta_t_now > 0 && delta_t_now <= 1000) {
                        Log.i(TAG, "Server is alive");
                        isConnected = isConnected && true;
                        return isConnected;
                    }
                }
                disconnect();
                isConnected = false;
                return isConnected;
            }else{
                return isConnected;
            }
        }else{
            disconnect();
            return isConnected;
        }
    }
}

